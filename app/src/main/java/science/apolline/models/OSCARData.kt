package science.apolline.models


import android.os.Parcel
import android.os.Parcelable

import com.google.gson.JsonArray
import com.google.gson.JsonObject

class OSCARData : IntfSensorData {
    var count: Int = 0
    val buff = IntArray(64)
    //Les données collectées par le capteur
    var co = 0f  //carbon monoxide
    var co_brut = 0f
    var concentration = 0f //PM2.5
    var h = 0f   //humidité
    var h_brut = 0f
    var lowcount = 0f
    var lv = 0f
    var no2 = 0f //no2
    var no2_brut = 0f
    var o3 = 0f //ozone
    var o3_brut = 0f
    var pinval = 0f
    var poucentagelow = 0f
    var t =0f //temperature
    var t_brut = 0f


    val LENG: Byte = 31




    var dateGPS: String = ""
    var AltiGPS: Double = 0.toDouble()
    var SpeedGPS: Double = 0.toDouble()
    var pression: Double = 0.toDouble()
    var tempe: Double = 0.toDouble()
    var humi: Double = 0.toDouble()
    var bat_volt: Double = 0.toDouble()
    var SatGPS: Int = 0

    constructor()

    protected constructor(`in`: Parcel) {
        co = `in`.readFloat()
        co_brut = `in`.readFloat()
        lv = `in`.readFloat()
        t = `in`.readFloat()
        t_brut = `in`.readFloat()
        pinval = `in`.readFloat()
        o3 = `in`.readFloat()
        o3_brut = `in`.readFloat()
        h = `in`.readFloat()
        h_brut = `in`.readFloat()
        lowcount=`in`.readFloat()
        poucentagelow = `in`.readFloat()
        concentration =`in`.readFloat()
        no2=`in`.readFloat()
        no2_brut=`in`.readFloat()

    }

    override fun describeContents(): Int {
        return 0
    }

    override fun writeToParcel(parcel: Parcel, i: Int) {
        parcel.writeFloat(co)
        parcel.writeFloat(co_brut)
        parcel.writeFloat(o3)
        parcel.writeFloat(o3_brut)
        parcel.writeFloat(no2)
        parcel.writeFloat(no2_brut)
        parcel.writeFloat(pinval)
        parcel.writeFloat(h)
        parcel.writeFloat(h_brut)
        parcel.writeFloat(lv)
        parcel.writeFloat(t)
        parcel.writeFloat(t_brut)
        parcel.writeFloat(lowcount)
        parcel.writeFloat(concentration)
    }

    fun checkValue(): Boolean {
        var receiveflag = false
        var receiveSum = 0

        for (i in 0 until LENG - 2)
            receiveSum = receiveSum + buff[i]
        receiveSum = receiveSum + 0x42

        if (receiveSum == (buff[LENG - 2] shl 8) + buff[LENG - 1])
            receiveflag = true
        return receiveflag
    }

    fun parse() {
    }

    private fun extract(buffer: IntArray, lhs: Int, rhs: Int) = (buffer[lhs] shl 8) + buffer[rhs]


    enum class Units constructor(val value: String) {
        CONCENTRATION_UG_M3("µg/m3"),
        CONCENTRATION_ABOVE("#/0.1L"),
        PERCENTAGE("%"),
        TEMPERATURE_CELSIUS("°C"),
        TEMPERATURE_KELVIN("°K");
    }


    private fun addNestedJsonArray(obj: JsonObject, property: String, value: Number, unit: Units) {
        val array = JsonArray()
        array.add(value.toDouble())
        array.add(unit.value)
        obj.add(property, array)
    }

    override fun toJson(): JsonObject {
        val obj = JsonObject()

        addNestedJsonArray(obj, "pm.2_5.value", concentration, Units.CONCENTRATION_UG_M3)
        addNestedJsonArray(obj, "co", co, Units.CONCENTRATION_UG_M3)
        addNestedJsonArray(obj, "no2", no2, Units.CONCENTRATION_UG_M3)
        addNestedJsonArray(obj, "o3", o3, Units.CONCENTRATION_UG_M3)
        addNestedJsonArray(obj, "temperature.c", t, Units.TEMPERATURE_CELSIUS)
        addNestedJsonArray(obj, "humidity", h, Units.PERCENTAGE)

        return obj
    }

    fun setBuff(position: Int, value: Int) {
        this.buff[position] = value
    }

    companion object CREATOR : Parcelable.Creator<OSCARData> {
        override fun createFromParcel(parcel: Parcel): OSCARData? {
            return OSCARData(parcel)
        }

        override fun newArray(size: Int): Array<OSCARData?> {
            return arrayOfNulls(size)
        }
    }
}
